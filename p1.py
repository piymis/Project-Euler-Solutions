target = 999

def sum_divisible_by_n(n):
    p = target//n
    return (n*(p*(p+1)))//2

print(sum_divisible_by_n(3) + sum_divisible_by_n(5) - sum_divisible_by_n(15))
