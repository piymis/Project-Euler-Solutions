# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 15:51:41 2018

@author: mishrap
"""

def is_palindrome(num):
    num_str = str(num)
    if len(num_str) == 0 or len(num_str) == 1:
        return True
    
    if num_str[0] != num_str[-1]:
        return False
    
    return is_palindrome(num_str[1:-1])

range_tuple = (999, 100, -1)

palindromes_list = []

for i in range(*range_tuple):
    for j in range(*range_tuple):
        n = i*j
        if is_palindrome(n):
#            print(n)
#            print(i , j)
            palindromes_list.append(n)
            break

print(max(palindromes_list))