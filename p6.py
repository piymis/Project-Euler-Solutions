# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 19:31:48 2018

@author: mishrap
"""

num_list = list(range(1,101))

sum_squares = sum(map(lambda x: x*x, num_list))

squares_sum = sum(num_list) ** 2

print(squares_sum-sum_squares)