# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 15:08:43 2018

@author: mishrap
"""

num =  600851475143 

import math
import itertools


l1= [2]
l2 = range(3, int(math.sqrt(num)), 2)


def generate_primes(num):
    number_list = [False, False] + list([True]*(num-1))
    for i in range(2, int(math.sqrt(num))+1):
        if number_list[i]:
            for j in range(i*i, num+1, i):
                number_list[j] = False
    return [x for x, y in enumerate(number_list) if y]
            
primes_list = generate_primes(int(math.sqrt(num)) + 1)

prime_factors_list = []
for i in primes_list:
    if num % i == 0:
        prime_factors_list.append(i)
        


print(prime_factors_list[-1])