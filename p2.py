import sys
target = 4e6

def fib(n):
    a,b = 1,2
    while a <= n:
        yield a
        a,b = b,a+b



def main():
    total = 0
    for value in fib(int(target)):
        if not value%2:
            total+=value

    print(total)

if __name__ == '__main__':
    main()

