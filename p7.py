# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 19:38:04 2018

@author: mishrap
"""

# TODO
import math

def generate_primes(num_times):
    
    number_list = [False, False] + list([True]*(num_times-1))
    for i in range(2, int(math.sqrt(num_times))+1):
        if number_list[i]:
            for j in range(i*i, num_times+1, i):
                number_list[j] = False
    return [x for x, y in enumerate(number_list) if y]

ge