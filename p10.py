#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 22:10:25 2018

@author: piyush
"""
import math

def generate_primes(num):
    number_list = [False, False] + list([True]*(num-1))
    for i in range(2, int(math.sqrt(num))+1):
        if number_list[i]:
            for j in range(i*i, num+1, i):
                number_list[j] = False
    return [x for x, y in enumerate(number_list) if y]


num = int(2e6)
print(sum(generate_primes(num)))