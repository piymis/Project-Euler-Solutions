# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 16:38:30 2018

@author: mishrap
"""

from functools import reduce

def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def lcm(a, b):
    return a*b // gcd(a, b)

def lcm_multi(args):
    return reduce(lcm, args)


print(lcm_multi(list(range(1,21))))